var TTShandler = new SpeechSynthesisUtterance();
var timer=0;    // individual timers


Vue.component('dictey-game', {

  template: `

  <div>
    <div class="version">{{version}}</div>
    <div class="title carton6 text-stroke">Dictée</div>

    <div class="wrapper firstpage" v-if="!gameStart">

        <div>
			Dans ce jeu il faut écrire les mots qui sont dictés. <br /> Attention, ce jeu ne marche qu'avec le navigateur Google Chrome !
		</div>


		<div class="spacer"></div>


        <table class="wrapper medium-font ubuntuM config-table">

            <tr align=left>
                <td class="carton6">Nombre de mots</td>
                <td><input type="number" step="1" min="10" max="999" v-model="words_count"></td>
                <td class='info'>Nombre de mots à écrire</td>
            </tr>

            <tr align=left>
                <td class="carton6">Mode</td>
                <td>
                    <label><input type=radio v-model=mode name=mode value=dictee> Dictée</label><br/>
                    <label><input type=radio v-model=mode name=mode value=copie> Copie</label>
                </td>
                <td class='info'>Le mode de jeu</td>
            </tr>


            <tr align=left>
                <td class="carton6">Catégories</td>
                <td>
                <select class="js-example-basic-multiple" name="tags[]" v-model=tags multiple="multiple">
                  <option v-for="tag in allTags" v-bind:value="tag">{{tag}}</option>

                </select>

                </td>
                <td class='info'>Les catégories de mots</td>
            </tr>


            <tr align=left>
                <td class="carton6">Changer de mot</td>
                <td><input type="number" v-model="timer" step="1" min="5" max="100" ></td>
                <td class='info'>Secondes avant de changer</td>
            </tr>



        </table>


		<div class="spacer"></div>

        <button class="button is-success carton6 big-font" @click="play()">Démarrer le jeu</button>

	</div>





    <div v-if='gameStart'>

        <!-- Stop the current game -->
        <div v-show="!theEnd" class="exit fa fa-times-circle" @click="stop()"> </div>

        <br />

        <div v-show="!theEnd && mode!='dictee'" class="word" @click="nextWord()">{{current_word}}</div>
        <div v-show="!theEnd && mode=='dictee'" class="word"><div class="bubble fa fa-commenting-o" @click="say(current_word)"></div></div>


        <!-- run the timer, until theEnd -->
        <div class="countdown" v-show="!theEnd">
            <div class="meter">  <span v-bind:style="{width: timerValuePercent + '%'}"></span>     </div>
        </div>


        <center v-show="theEnd">
            <div> <div v-for="w in wordsList">{{w}}</div> </div>
            <br />
            <button class="button is-danger carton6 big-font" @click="exit()">Retour au menu</button>
        </center>

    </div>

  </div>

  `,

    data: function() {
        return {
            version: "20180503-14:33",         
            gameStart:false,
            theEnd: false,  // end of the game

            timer:15,       // time until next word
            timerValue:0,   // current timer value

            words_count:5,  // how many words to write
            words: [],      // words in use in the game
            wordsList: [],      // words in use in the game
            current_word: "", // the word currently displayed

            allWords: [],   // all the words in the game

            allTags: [],    // all tags
            tags: [],       // selected tags

            mode: "copie",


        }
    },

    computed: {

        timerValuePercent: function () {
            return ((this.timerValue * 10) / this.timer).toFixed(2);
        }

    },

    methods: {


        say: function (src) {

    		TTShandler.text = src;
    		TTShandler.rate = 1;
    		TTShandler.pitch = 1;
    		TTShandler.lang = 'fr-FR';
    		speechSynthesis.speak(TTShandler);
        },



        // display next word
        nextWord: function () {

            if (this.words.length > 1) {

                // put it in the completed list
                this.wordsList.push(this.words[0].word);    
                
                // remove current element
                this.words.splice(0, 1);

                this.current_word = this.words[0].word;

                // say it !
                this.say (this.current_word);

                this.timerValue = this.timer*10;    // reset timer

            }
            else {

                // put it in the completed list
                this.wordsList.push(this.words[0].word); 
                this.theEnd = true;
            }

        },


        // get all tags
        getAllTags: function () {
            var allLines = JSON.parse(localStorage.getItem("data-dictey")).game.words;

            var alltags = [];

            for (var i=0 ; i < allLines.length ; i++) {


                if (allLines[i].tags.trim() != "") {

                    // split and trim
                    var splited = allLines[i].tags.split(" ");

                    // add the split parts
                    for (var j=0 ; j<splited.length ; j++) {

                        // add only unique items
                        if (alltags.indexOf(splited[j].trim()) == -1)
                            alltags.push (splited[j].trim());
                    }

                }
            }

            this.allTags = alltags;

        },



        // stop gameStart
        stop: function () {
            this.theEnd = true;
        },



        // back to menu and init
        exit: function () {
            this.gameStart = false;
            this.theEnd = false;
            this.timerValue = 0;
            this.words = [];
            this.wordsList = [];
            this.current_word = "";

            clearInterval(timer);
        },



        // run the game
        play: function () {

            // save the parameters
            this.saveConfig();

            this.gameStart = true;
            this.theEnd = false;

            this.allWords = JSON.parse(localStorage.getItem("data-dictey")).game.words;

            // filter by tags
            if (this.tags.length > 0) {

                var selected = this.tags;

                this.allWords = $.grep( this.allWords, function( n, i ) {
                    return ($.inArray(n.tags, selected) !== -1);
                });
            }


            for (var i=0 ; i < this.words_count ; i++) {
                this.words.push(this.getRandomInArray(this.allWords));
            }


            // display first word
            this.current_word = this.words[0].word;
           
            // say it !
            this.say (this.current_word);

            // start timer for this word
            this.startTimer();


        },


        // get a random item in an array
        getRandomInArray: function (array) {
            
            var idx = Math.floor(Math.random() * array.length );
            return array[idx];
        },


        // start the timer
        startTimer: function() {

            // init the timer
            this.timerValue = this.timer*10;

            timer = window.setInterval(() => {

                if (!this.theEnd)
                    this.timerValue -= 1;

                if (this.timerValue <= 0 && this.gameStart) {
                    this.nextWord();
                }
            }, 100)
        },



        // save config in localStorage
        saveConfig: function () {
            /*
                        var toSave = {
                            keyboard_layout:this.keyboard_layout,
                            lettersRange:this.lettersRange,
                            template:this.template,
                            blockError:this.blockError,
                            randomLetters:this.randomLetters,
                            showKeyboard:this.showKeyboard
                        };
            
                        localStorage.setItem("tapelexis_conf", JSON.stringify(toSave));
            */
                    },
            
            
            
            
            
                    // load config from localStorage
                    loadConfig: function () {
            /*
                        var loadconf = JSON.parse(localStorage.getItem("tapelexis_conf"));
            
                        if (loadconf) {
                            this.keyboard_layout = loadconf.keyboard_layout;
                            this.lettersRange = loadconf.lettersRange;
                            this.template = loadconf.template;
                            this.blockError = loadconf.blockError;
                            this.randomLetters = loadconf.randomLetters;
                            this.showKeyboard = loadconf.showKeyboard;
                        }
            
            */
                    },
  


        // generate the text to write (letters, sentence, text...)
        genText: function () {
            /*
                        // init
                        this.array_textInput = [];
            
            
                        // random letters
                        if (this.randomLetters) {
            
                            for (var i=0 ; i<=this.letters_count ; i++) {
            
                                var letter = this.array_lettersRange;
            
                                if (letter == " ") letter="&nbps;"
            
                                this.array_textInput.push( this.getRandomInArray( letter ));
                            }
                        }
            
                        // direct text, no random
                        else {
            
                            // split on every char
                            this.array_textInput = this.lettersRange.split('');
            
                            // set the limit to the input letters count
                            this.letters_count = this.array_textInput.length -1;
                        }
            
            */
                    },
                      




    }, // /methods



    mounted: function() {

        // fill the tags
        this.getAllTags();

        // load the config
        //this.loadConfig();


    } // end mounted









})
